<?php

/**
 * @file
 * IndieWeb test module file.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Implements hook_theme().
 */
function indieweb_test_theme($existing, $type, $theme, $path) {
  return [
    'post_type_like_twitter' => ['variables' => ['target_url' => NULL]],
    'post_type_like_site' => ['variables' => ['target_url' => NULL]],
    'post_type_reply_twitter' => ['variables' => ['target_url' => NULL]],
    'post_type_reply_photo_twitter' => ['variables' => ['target_url' => NULL]],
    'post_type_reply_site' => ['variables' => ['target_url' => NULL]],
    'post_type_reply_fediverse' => ['variables' => ['target_url' => NULL]],
    'post_type_repost_twitter' => ['variables' => ['target_url' => NULL]],
    'post_type_mention_twitter' => ['variables' => ['target_url' => NULL]],
    'post_type_bookmark_site' => ['variables' => ['target_url' => NULL]],
    'post_type_follow_fediverse' => ['variables' => ['target_url' => NULL]],
    'post_type_rsvp_site' => ['variables' => ['target_url' => NULL]],
    'feed_pinned' => ['variables' => []],
  ];
}

/**
 * Implements hook_indieweb_micropub_node_pre_create_alter().
 */
function indieweb_test_indieweb_micropub_node_pre_create_alter(&$values, &$payload) {
  if (isset($payload['act_on_hooks'])) {
    $values['title'] = 'Title set from hook';
  }
}

/**
 * Implements hook_indieweb_micropub_node_saved().
 */
function indieweb_test_indieweb_micropub_node_saved(NodeInterface $node, $values, $payload, $payload_original) {
  if (isset($payload_original['act_on_hooks'])) {
    // Just create another node with the same label.
    $second_node = $node->createDuplicate();
    $second_node->setTitle('duplicated node');
    $second_node->save();
  }
}

/**
 * Implements hook_indieweb_micropub_no_post_made().
 *
 * @param $payload
 *
 * @return \Drupal\Core\GeneratedUrl|string
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 * @throws \Drupal\Core\Entity\EntityMalformedException
 */
function indieweb_test_indieweb_micropub_no_post_made($payload) {
  if (isset($payload['no_post_made'])) {
    $values = [
      'uid' => 1,
      'status' => 1,
      'type' => 'page',
      'title' => $payload['random_title'][0],
    ];
    $node = Node::create($values);
    $node->set('body', $payload['random_content']);
    $node->save();
    if ($node->id()) {
      return $node->toUrl('canonical', ['absolute' => TRUE])->toString();
    }
  }
}

/**
 * Alter the manage fields reuse form.
 *
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 *
 * @return void
 */
function indieweb_test_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'field_ui_field_storage_reuse_form') {
    foreach (Element::children($form['add']['table']) as $key) {
      $form['add']['table'][$key]['operations']['#attributes']['class'][] = $form['add']['table'][$key]['operations']['#name'] . '_reuse_button';
    }
  }
}

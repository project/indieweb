<?php

namespace Drupal\indieweb_microsub\Reader;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\indieweb_microsub\Entity\MicrosubChannelInterface;
use Drupal\reader\ReaderBase;

class Reader extends ReaderBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getChannels() {
    if ($this->microsubIsInternal() && \Drupal::currentUser()->hasPermission('access reader')) {

      $ids = \Drupal::entityTypeManager()
        ->getStorage('indieweb_microsub_channel')
        ->getQuery()
        ->accessCheck()
        ->condition('status', 1)
        ->sort('weight')
        ->execute();

      $channels = [];

      $notifications = \Drupal::entityTypeManager()->getStorage('indieweb_microsub_item')->getUnreadCountByChannel(0);
      $channels[] = (object) [
        'uid' => 'notifications',
        'name' => 'Notifications',
        'unread' => (int) $notifications,
      ];

      /** @var \Drupal\indieweb_microsub\Entity\MicrosubChannelInterface $channel */
      foreach (\Drupal::entityTypeManager()->getStorage('indieweb_microsub_channel')->loadMultiple($ids) as $channel) {
        $unread = 0;

        $indicator = $channel->getReadIndicator();
        if ($indicator == MicrosubChannelInterface::readIndicatorCount) {
          $unread = (int) $channel->getUnreadCount();
        }
        elseif ($indicator == MicrosubChannelInterface::readIndicatorNew) {
          $unread = (bool) $channel->getUnreadCount();
        }

        $channels[] = (object) [
          'uid' => $channel->id(),
          'name' => $channel->label(),
          'unread' => $unread,
        ];
      }

      return [
        'label' => t('IndieWeb'),
        'channels' => $channels,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSourcesPage($op) {

    if (!($this->microsubIsInternal() && \Drupal::currentUser()->hasPermission('access reader'))) {
      return ['#markup' => '<div class="general-content">' . $this->t('You can not managed IndieWeb sources.') . '</div>'];
    }

    switch ($op) {
      case 'add-source':
        $build = $this->addSourcesButton($this->t('Back to sources'), 'indieweb-microsub');
        $source = \Drupal::entityTypeManager()->getStorage('indieweb_microsub_source')->create();
        $form = \Drupal::service('entity.form_builder')->getForm($source, 'add');
        $form['#action'] .= '?destination=' . $this->getSourcesListUrl('indieweb-microsub');
        $build['add'] = $form;
        break;

      case 'add-channel':
        $build = $this->addSourcesButton($this->t('Back to sources'), 'indieweb-microsub');
        $source = \Drupal::entityTypeManager()->getStorage('indieweb_microsub_channel')->create();
        $form = \Drupal::service('entity.form_builder')->getForm($source, 'add');
        $form['#action'] .= '?destination=' . $this->getSourcesListUrl('indieweb-microsub');
        $build['add'] = $form;
        break;

      case 'edit':
        $type = $_GET['type'] ?? 'indieweb_microsub_source';
        $build = $this->addSourcesButton($this->t('Back to sources'), 'indieweb-microsub');
        $source = \Drupal::entityTypeManager()->getStorage($type)->load($_GET['id']);
        $form = \Drupal::service('entity.form_builder')->getForm($source, 'edit');
        $form['#action'] .= '&destination=' . $this->getSourcesListUrl('indieweb-microsub');
        $form['actions']['delete']['#access'] = FALSE;
        $build['edit'] = $form;
        break;

      case 'delete':
        $type = $_GET['type'] ?? 'indieweb_microsub_source';
        $build = $this->addSourcesButton($this->t('Back to sources'), 'indieweb-microsub');
        $source = \Drupal::entityTypeManager()->getStorage($type)->load($_GET['id']);
        $form = \Drupal::service('entity.form_builder')->getForm($source, 'delete');
        $form['actions']['cancel']['#access'] = FALSE;
        $form['#action'] .= '&destination=' . $this->getSourcesListUrl('indieweb-microsub');
        $build['delete'] = $form;
        $build['delete']['#prefix'] = $this->addSourcesConfirmDeleteText($source->label());
        break;

      default:
        $add_source = $this->addSourcesButton($this->t('+ Add source'), 'indieweb-microsub', 'add-source');
        $add_channel = $this->addSourcesButton($this->t('+ Add channel'), 'indieweb-microsub', 'add-channel');
        $build['add_source'] = $add_source;
        $build['add_channel'] = $add_channel;

        $rows = [];
        /** @var \Drupal\indieweb_microsub\Entity\MicrosubSourceInterface[] $sources */
        $channel_ids = \Drupal::entityTypeManager()->getStorage('indieweb_microsub_channel')->getQuery()
          ->accessCheck()
          ->sort('weight', 'ASC')->execute();
        $channels = \Drupal::entityTypeManager()->getStorage('indieweb_microsub_channel')->loadMultiple($channel_ids);
        foreach ($channels as $channel) {
          $row = [];
          $row[] = $this->t('Channel: @name', ['@name' => $channel->label()]);
          $row[] = Link::fromTextAndUrl($this->t('Edit'), Url::fromRoute('reader.sources', [
            'module' => 'indieweb-microsub',
            'op' => 'edit',
          ], ['query' => ['id' => $channel->id(), 'type' => 'indieweb_microsub_channel']]))->toString();
          $row[] = Link::fromTextAndUrl($this->t('Delete'), Url::fromRoute('reader.sources', [
            'module' => 'indieweb-microsub',
            'op' => 'delete',
          ], ['query' => ['id' => $channel->id(), 'type' => 'indieweb_microsub_channel']]))->toString();
          $rows[] = $row;

          $sources = \Drupal::entityTypeManager()->getStorage('indieweb_microsub_source')->loadByProperties(['channel_id' => $channel->id()]);
          foreach ($sources as $source) {
            $row = [];
            if (!empty($source->getName())) {
              $label = $source->getName();
            }
            else {
              $label = Unicode::truncate($source->label(), 40, FALSE, TRUE);
            }
            $row[] = $label;
            $row[] = Link::fromTextAndUrl($this->t('Edit'), Url::fromRoute('reader.sources', [
              'module' => 'indieweb-microsub',
              'op' => 'edit',
            ], ['query' => ['id' => $source->id()]]))->toString();
            $row[] = Link::fromTextAndUrl($this->t('Delete'), Url::fromRoute('reader.sources', [
              'module' => 'indieweb-microsub',
              'op' => 'delete',
            ], ['query' => ['id' => $source->id()]]))->toString();
            $rows[] = $row;
          }
        }

        $build['table'] = [
          '#type' => 'table',
          '#rows' => $rows,
          '#empty' => $this->t('No sources available'),
          '#attributes' => ['class' => ['reader-content-table']],
        ];

        break;
    }

    $build['#title'] = $this->t('Manage IndieWeb');
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeline($id, $search = NULL) {
    if ($this->microsubIsInternal() && \Drupal::currentUser()->hasPermission('access reader')) {
      \Drupal::request()->request->set('channel', $id);
      if (isset($_SESSION['microsub_status']) && empty($search)) {
        \Drupal::request()->request->set('is_read', 0);
      }
      /** @var \Drupal\indieweb_microsub\MicrosubClient\MicrosubClientInterface $client */
      $client = \Drupal::service('indieweb.microsub.client');
      $response = $client->getTimeline(TRUE, $search);
      if (!empty($response['response'])) {
        return $response['response'];
      }
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTimelineActions($id) {
    $status_title = isset($_SESSION['microsub_status']) ? $this->t('View all items') : $this->t('View unread items');

    return [
      ['action' => 'mark-read', 'title' => $this->t('Mark read')],
      ['action' => 'status', 'title' => $status_title]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function doTimelineAction($action, $id) {
    if ($this->microsubIsInternal() && \Drupal::currentUser()->isAuthenticated()) {
      if ($action == 'mark-read') {
        if ($id == 'notifications') {
          $id = 0;
        }
        \Drupal::entityTypeManager()->getStorage('indieweb_microsub_item')->changeReadStatus($id, 1, []);
      }
      if ($action == 'status') {
        if (isset($_SESSION['microsub_status'])) {
          unset($_SESSION['microsub_status']);
        }
        else {
          $_SESSION['microsub_status'] = TRUE;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPostActions($id, $item) {
    $action = 'mark-unread';
    $title = $this->t('Mark unread');
    if (isset($item->_is_read) && !$item->_is_read) {
      $action = 'mark-read';
      $title = $this->t('Mark read');
    }

    return [[
      'action' => $action,
      'title' => $title,
    ]];
  }

  /**
   * {@inheritdoc}
   */
  public function doPostAction($action, $id, $items) {
    if ($this->microsubIsInternal() && \Drupal::currentUser()->isAuthenticated()) {
      if ($action == 'mark-read' || $action == 'mark-unread') {
        if ($id == 'notifications') {
          $id = 0;
        }
        $status = $action == 'mark-read' ? 1 : 0;
        \Drupal::entityTypeManager()->getStorage('indieweb_microsub_item')->changeReadStatus($id, $status, $items);
      }
    }
  }

  /**
   * Returns whether the internal microsub channel is used.
   *
   * @return array|mixed|null
   */
  protected function microsubIsInternal() {
    return \Drupal::config('indieweb_microsub.settings')->get('microsub_internal');
  }

}
